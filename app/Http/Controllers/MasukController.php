<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MasukController extends Controller
{
    public function berhasil()
    {
        return 'Berhasil Masuk';
    }

    public function admin()
    {
        return 'Admin Berhasil Masuk';
    }

}
