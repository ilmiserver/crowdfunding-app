<?php

namespace App\Listeners;

use App\Events\UserRegisteredEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\SendOtpCodeMail;
use Mail;

class SendEmailOtpCode implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegisteredEvent  $event
     * @return void
     */
    public function handle(UserRegisteredEvent $event) //karena yg menerima adalah $event //sehingga ditulis $event->user
    {
        if ($event->condition == 'register'){
            $pesan = "We are exited to have you get started. First, you need to confirm your account. This is your OTP Code: ";
        }
        elseif ($event->condition == 'regenerate'){
            $pesan = "Regenerate OTP Successfull. This is your OTP Code :";
        }

        Mail::to($event->user)->send(new SendOtpCodeMail($event->user, $pesan));
    }
}
